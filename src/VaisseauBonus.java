public class VaisseauBonus {
    /** Cet attribut modélise la position du vaisseau bonus concernant la hauteur de la fenêtre */
    private double positionX;
    /** Cet attribut modélise la position du vaisseau bonus concernant la largeur de la fenêtre */
    private double positionY;
    /** Cet attribut modélise le dessin du vaisseau bonus */
    private EnsembleChaines dessin;
    /** Cet attribut modélise le compteur de tours */
    private int compteurTours;
    /** permet de construire le vaisseau bonus
     * @param positionX la position du vaisseau bonus concernant la hauteur de la fenêtre
     * @param positionY la position du vaisseau bonus concernant la largeur de la fenêtre
     */
    public VaisseauBonus(double positionX, double positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.dessin = new EnsembleChaines();
        this.compteurTours = 0;
    }
    /** permet de récupérer la position du vaisseau bonus concernant la hauteur de la fenêtre
     * @return la position du vaisseau bonus concernant la hauteur de la fenêtre
     */
    public double getPositionX() {
        return this.positionX;
    }
    /** permet de récupérer la position du vaisseau bonus concernant la largeur de la fenêtre
     * @return la position du vaisseau bonus concernant la largeur de la fenêtre
     */
    public double getPositionY() {
        return this.positionY;
    }
    /** permet de récupérer le dessin du vaisseau bonus
     * @return le dessin du vaisseau bonus
     */
    public EnsembleChaines getEnsembleChaines() {
        this.dessin = new EnsembleChaines();
        this.dessin.ajouteChaine((int) positionX, (int) positionY, "     ▄▄████▄▄     ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-1, "   ▄██████████▄   ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-2, " ▄██▄██▄██▄██▄██▄ ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-3, "   ▀█▀  ▀▀  ▀█▀   ");
        return this.dessin;
    }
    /** permet de déplacer le vaisseau bonus
     * @param dx la distance de déplacement concernant la hauteur de la fenêtre
     * @param dy la distance de déplacement concernant la largeur de la fenêtre
     */
    public void evolue() {
        this.compteurTours++;
        if (this.compteurTours % 2 == 0) {
            this.positionX -= 0.7;
            for (ChainePositionnee chaine : this.dessin.chaines) {
                chaine.x = (int) this.positionX; 
            }
        }
    }
    /** permet de savoir si le vaisseau a été touché par un projectile
     * @param x la position du projectile concernant la hauteur de la fenêtre
     * @param y la position du projectile concernant la largeur de la fenêtre
     * @return true si le vaisseau a été touché par un projectile, false sinon
     */
    public boolean contient(int x, int y) {
        return this.dessin.contient(x,y);
    }
}
