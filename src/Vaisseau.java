public class Vaisseau {
    /** Cet attribut modélise la position du vaisseau concernant la hauteur de la fenêtre */ 
    private double posX;
    /** Cet attribut modélise le dessin du vaisseau */
    private EnsembleChaines dessin;
    /** permet de construire le vaisseau
     * @param posX la position du vaisseau concernant la hauteur de la fenêtre
     */
    public Vaisseau(double posX) {
        this.dessin = new EnsembleChaines();
        this.posX = posX;
    }
    /** permet de récupérer la position du vaisseau concernant la hauteur de la fenêtre
     * @return la position du vaisseau concernant la hauteur de la fenêtre
     */
    public double getPosX() {
        return this.posX;
    }
    /** permet de récupérer le dessin du vaisseau
     * @return le dessin du vaisseau
     */
    public EnsembleChaines getEnsembleChaines() {
        this.dessin = new EnsembleChaines();
        this.dessin.ajouteChaine((int)posX, 5, "      ▄      ");
        this.dessin.ajouteChaine((int)posX, 4, "     ███     ");
        this.dessin.ajouteChaine((int)posX, 3, "▄███████████▄");
        this.dessin.ajouteChaine((int)posX, 2, "█████████████");
        this.dessin.ajouteChaine((int)posX, 1, "█████████████");
        return this.dessin;
    }
    /** permet de déplacer le vaisseau
     * @param dx la distance de déplacement
     */
    public void deplace(double dx) {
        this.posX += dx;
        for (ChainePositionnee chaine : this.dessin.chaines) {
            chaine.x = (int) this.posX; 
        }
    }
    /** permet d'obtenir la position du canon du vaisseau
     * @return la position du canon du vaisseau
     */
    public double positionCanon() {
        return this.posX + 6;
    }
    /** permet de savoir si le vaisseau a été touché par un projectile d'alien
     * @param x la position du projectile concernant la hauteur de la fenêtre
     * @param y la position du projectile concernant la largeur de la fenêtre
     * @return true si le vaisseau a été touché par un projectile, false sinon
     */
    public boolean contient(int x, int y) {
        return this.dessin.contient(x, y);
    }
}
