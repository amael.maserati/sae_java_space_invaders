public class AlienDifficile {
    /** cet attribut modélise la position de l'alien concernant la hauteur de la fenêtre */
    private double positionX;
    /** cet attribut modélise la position de l'alien concernant la largeur de la fenêtre */
    private double positionY;
    /** cet attribut modélise le dessin de l'alien */
    private EnsembleChaines dessin;
    /** cet attribut modélise la direction du déplacement de l'alien */
    private double direction;
    /** cet attribut modélise un compteur de tours */
    private int compteurTours;
    /** cet attribut modélise l'animation de l'alien */
    private boolean sAnime;
    /** permet de construire l'alien
     * @param positionX la position de l'alien concernant la hauteur de la fenêtre
     * @param positionY la position de l'alien concernant la largeur de la fenêtre
     */
    public AlienDifficile(double positionX, double positionY) {
        this.dessin = new EnsembleChaines();
        this.positionX = positionX;
        this.positionY = positionY;
        this.direction = 0.1;
        this.compteurTours = 0;
        this.sAnime = false;
    }
    /** permet de récupérer le dessin de l'alien
     * @return le dessin de l'alien
     */
    public EnsembleChaines getEnsembleChaines() {
        this.dessin = new EnsembleChaines();
        this.dessin.ajouteChaine((int) positionX, (int) positionY-1, "    ▄█▀███▀█▄    ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-2, "   █ █▀▀▀▀▀█ █   ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-3, "   █ █▀▀▀▀▀█ █   ");
        if (this.sAnime) {
            this.dessin.ajouteChaine((int) positionX, (int) positionY, "      ▀▄   ▄▀     ");
            this.dessin.ajouteChaine((int) positionX, (int) positionY-4, "      ▀▀ ▀▀      ");
        } else {
            this.dessin.ajouteChaine((int) positionX, (int) positionY, "    ▀▄   ▄▀     ");
            this.dessin.ajouteChaine((int) positionX, (int) positionY-4, "   ▀▀       ▀▀      ");
        }
        return this.dessin;
    }
    /** permet de récupérer la position de l'alien concernant la hauteur de la fenêtre
     * @return la position de l'alien concernant la hauteur de la fenêtre
     */
    public double getPositionX() {
        return this.positionX;
    }
    /** permet de récupérer la position de l'alien concernant la largeur de la fenêtre
     * @return la position de l'alien concernant la largeur de la fenêtre
     */
    public double getPositionY() {
        return this.positionY;
    }
    /** permet de faire évoluer l'alien */
    public void evolue() {
        if (this.compteurTours == 80) {
            this.compteurTours = 0;
            --this.positionY;
            direction = -direction;
            for (int i = 0; i < this.dessin.chaines.size(); ++i) {
                this.dessin.chaines.get(i).x = (int) this.positionX;
                this.dessin.chaines.get(i).y = i == 0 ? (int) this.positionY : (int) this.positionY - i; 
            }
        }
        if (this.compteurTours % 10 == 0) {
            this.sAnime = !this.sAnime;
        }
        this.positionX += direction;
        for (ChainePositionnee c : this.dessin.chaines) {
            c.x = (int) this.positionX;
        }
        ++this.compteurTours;
    }
    /** permet de savoir si l'élément d'un dessin de l'alien contient un autre dessin
     * @param x la position de l'élément du dessin concernant la hauteur de la fenêtre
     * @param y la position de l'élément du dessin concernant la largeur de la fenêtre
     * @return true si l'alien contient l'élément du dessin, false sinon
     */
    public boolean contient(int x, int y) {
        return this.dessin.contient(x, y);
    }
}
