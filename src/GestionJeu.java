import java.util.ArrayList;
import java.util.List;

public class GestionJeu {
    /** Cet attribut modélise la hauteur de la fenêtre */
    private int hauteur;
    /** Cet attribut modélise la largeur de la fenêtre */
    private int largeur;
    /** Cet attribut modélise les chaines à afficher */
    private EnsembleChaines lesChaines;
    /** Cet attribut modélise le vaisseau */
    private Vaisseau vaisseau;
    /** Cet attribut modélise une liste de projectiles */
    private List<Projectile> projectiles;
    /** Cet attribut modélise une liste de projectiles touchés */
    private List<Projectile> projectilesTouches;
    /** Cet attribut modélise une liste de projectiles ratés */
    private List<Projectile> projectilesRates;
    /** Cet attribut modélise le score du joueur */
    private Score score;
    /** Cet attribut modélise une liste d'aliens */
    private List<Alien> aliens;
    /** Cet attribut modélise une liste d'aliens touchés */
    private List<Alien> aliensTouches;
    /** Cet attribut définit si la partie est perdue ou non */
    private boolean isOver;
    /** Cet attribut définit si le vaisseau peut tirer ou non */
    private boolean peutTirer;
    /** Cet attribut modélise le temps d'attente entre deux tirs */
    private int tempsDAttenteTir;
    /** Cet attribut modélise une liste de lasers */
    private List<Laser> lasers;
    /** Cet attribut modélise le nombre de lasers qui peuvent être tirés */
    private int nbTirsLaser;
    /** Cet attribut modélise la liste des lasers touchés */
    private List<Laser> lasersTouches;
    /** Cet attribut modélise la liste des lasers touchés */
    private List<Laser> lasersRates;
    /** Cet attribut modélise la liste des aliens touchés par un laser */
    private List<Alien> aliensTouchesParLaser;
    /** Cet attribut modélise la liste des aliens tireurs */
    private List<AlienTireur> aliensTireurs;
    /** Cet attribut modélise la liste des aliens tireurs touchés par un projectile */
    private List<AlienTireur> aliensTireursTouches;
    /** Cet attribut modélise la liste des aliens tireurs touchés par un laser */
    private List<AlienTireur> aliensTireursTouchesParLaser;
    /** Cet attribut modélise la liste des projectiles touchés par les aliens tireurs */
    private List<ProjectileAlienTireur> projectilesAlienTireurTouches;
    /** Cet attribut modélise si le laser a été utilisé ou non */
    private boolean laserUtilise;
    /** Cet attribut modélise la vie du personnage */
    private Vie vie;
    /** Cet attribut modélise le temps d'attente pour lancer le premier vaisseau bonus */
    private int tempsDAttenteVaisseauBonus1;
    /**  Cet attribut modélise le temps d'attente pour lancer le second vaisseau bonus */
    private int tempsDAttenteVaisseauBonus2;
    /** Cet attribut modélise s'il faut lancer ou non le premier vaisseau bonus */
    private boolean lancerVB1;
    /** Cet attribut modélise s'il faut lancer le deuxième vaisseau bonus */
    private boolean lancerVB2;
    /** Cet attribut modélise la liste des vaisseaux bonus */
    private List<VaisseauBonus> vaisseauxBonus;
    /** Cet attribut modélise la liste des vaisseaux bonus touchés par un projectile */
    private List<VaisseauBonus> vaisseauxBonusTouches;
    /** Cet attribut modélise la liste des vaisseaux bonus touchés par un laser */
    private List<VaisseauBonus> vaisseauxBonusTouchesParLaser;
    /** Cet attribut modélise un numéro correspondant au choix de la difficulté */
    private int choixDifficulte;
    /** Cet attribut modélise une liste de projectiles moyens */
    private List<ProjectileMoyen> projectilesMoyens;
    /** Cet attribut modélise une liste de projectiles moyens touchés */
    private List<ProjectileMoyen> projectilesMoyensTouches;
    /** Cet attribut modélise une liste de projectiles moyens ratés */
    private List<ProjectileMoyen> projectilesMoyensRates;
    /** Cet attribut modélise une liste d'aliens moyens */
    private List<AlienMoyen> aliensMoyen;
    /** Cet attribut modélise une liste d'aliens moyens touchés */
    private List<AlienMoyen> aliensMoyenTouches;
    /** Cet attribut modélise la liste des aliens moyens touchés par un laser */
    private List<AlienMoyen> aliensMoyensTouchesParLaser;
    /** Cet attribut modélise la liste des aliens tireurs moyens */
    private List<AlienTireurMoyen> aliensTireursMoyens;
    /** Cet attribut modélise la liste des aliens tireurs moyens touchés par un projectile */
    private List<AlienTireurMoyen> aliensTireursMoyensTouches;
    /** Cet attribut modélise la liste des aliens tireurs moyens touchés par un laser */
    private List<AlienTireurMoyen> aliensTireursMoyensTouchesParLaser;
    /** Cet attribut modélise la liste des projectiles touchés par les aliens tireurs moyen */
    private List<ProjectileAlienTireurMoyen> projectilesAlienTireurMoyenTouches;
    /** Cet attribut modélise si la difficulté choisie est la facile */
    private boolean difficulteFacileChoisie;
    /** Cet attribut modélise si la difficulté choisie est la moyenne */
    private boolean difficulteMoyenneChoisie;
    /** Cet attribut modélise si la difficulté choisie est la difficile */
    private boolean difficulteDifficileChoisie;
    /** Cet attribut modélise une liste de projectiles difficiles */
    private List<ProjectileDifficile> projectilesDifficiles;
    /** Cet attribut modélise une liste de projectiles difficiles touchés */
    private List<ProjectileDifficile> projectilesDifficilesTouches;
    /** Cet attribut modélise une liste de projectiles difficiles ratés */
    private List<ProjectileDifficile> projectilesDifficilesRates;
    /** Cet attribut modélise une liste d'aliens difficiles */
    private List<AlienDifficile> aliensDifficiles;
    /** Cet attribut modélise une liste d'aliens difficiles touchés */
    private List<AlienDifficile> aliensDifficilesTouches;
    /** Cet attribut modélise la liste des aliens difficiles touchés par un laser */
    private List<AlienDifficile> aliensDifficilesTouchesParLaser;
    /** Cet attribut modélise la liste des aliens tireurs difficiles */
    private List<AlienTireurDifficile> aliensTireursDifficiles;
    /** Cet attribut modélise la liste des aliens tireurs difficiles touchés par un projectile */
    private List<AlienTireurDifficile> aliensTireursDifficilesTouches;
    /** Cet attribut modélise la liste des aliens tireurs difficiles touchés par un laser */
    private List<AlienTireurDifficile> aliensTireursDifficilesTouchesParLaser;
    /** Cet attribut modélise la liste des projectiles touchés par les aliens tireurs difficiles */
    private List<ProjectileAlienTireurDifficile> projectilesAlienTireurDifficileTouches;
    /** Cet attribut modélise la liste contenant l'alien rapide */
    private List<AlienRapide> alienRapide;
    /** Cet attribut modélise la liste de l'alien rapide touché par un projectile */
    private List<AlienRapide> alienRapideTouche;
    /** Cet attribut modélise l'alien rapide touché par un laser */
    private List<AlienRapide> alienRapideToucheParLaser;
    /** Permet de construire le jeu */
    public GestionJeu() {
        this.hauteur = 60;
        this.largeur = 100;
        this.lesChaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(43);
        this.projectiles = new ArrayList<Projectile>();
        this.projectilesTouches = new ArrayList<Projectile>();
        this.projectilesRates = new ArrayList<Projectile>();
        this.score = new Score();
        this.isOver = false;
        this.peutTirer = true;
        this.aliens = new ArrayList<Alien>();
        for(int i = 0; i < 2; ++i){
            for(int j = 0; j < 5; ++j){
                this.aliens.add(new Alien(18 * j + 5, this.hauteur - (i + 3) * 6));
            }
        }
        this.aliensTouches = new ArrayList<Alien>(); 
        this.tempsDAttenteTir = 0;
        this.lasers = new ArrayList<Laser>();
        this.nbTirsLaser = 0;
        this.lasersTouches = new ArrayList<Laser>();
        this.lasersRates = new ArrayList<Laser>();
        this.aliensTouchesParLaser = new ArrayList<Alien>();
        this.aliensTireurs = new ArrayList<AlienTireur>();
        for (int i = 0; i < 1; ++i) {
            for (int j = 0; j < 4; ++j) {
                this.aliensTireurs.add(new AlienTireur(22 * j + 8, this.hauteur - (i + 2) * 6));
            }
        }
        this.aliensTireursTouches = new ArrayList<AlienTireur>();
        this.aliensTireursTouchesParLaser = new ArrayList<AlienTireur>();
        this.projectilesAlienTireurTouches = new ArrayList<ProjectileAlienTireur>();
        this.laserUtilise = false;
        this.vie = new Vie(2);
        this.tempsDAttenteVaisseauBonus1 = 0;
        this.tempsDAttenteVaisseauBonus2 = 0;
        this.lancerVB1 = false;
        this.lancerVB2 = false;
        this.vaisseauxBonus = new ArrayList<VaisseauBonus>();
        this.vaisseauxBonus.add(new VaisseauBonus(this.largeur + 1, this.hauteur - 5));
        this.vaisseauxBonus.add(new VaisseauBonus(this.largeur + 1, this.hauteur - 13));
        this.vaisseauxBonusTouches = new ArrayList<VaisseauBonus>();
        this.vaisseauxBonusTouchesParLaser = new ArrayList<VaisseauBonus>();
        this.choixDifficulte = 0;
        this.projectilesMoyens = new ArrayList<ProjectileMoyen>();
        this.projectilesMoyensTouches = new ArrayList<ProjectileMoyen>();
        this.projectilesMoyensRates = new ArrayList<ProjectileMoyen>();
        this.aliensMoyen = new ArrayList<AlienMoyen>();
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 5; ++j) {
                this.aliensMoyen.add(new AlienMoyen(18 * j + 5, this.hauteur - (i + 3) * 6));
            }
        }
        this.aliensMoyenTouches = new ArrayList<AlienMoyen>();
        this.aliensMoyensTouchesParLaser = new ArrayList<AlienMoyen>();
        this.aliensTireursMoyens = new ArrayList<AlienTireurMoyen>();
        for (int i = 0; i < 1; ++i) {
            for (int j = 0; j < 4; ++j) {
                this.aliensTireursMoyens.add(new AlienTireurMoyen(22 * j + 8, this.hauteur - (i + 2) * 6));
            }
        }
        this.aliensTireursMoyensTouches = new ArrayList<AlienTireurMoyen>();
        this.aliensTireursMoyensTouchesParLaser = new ArrayList<AlienTireurMoyen>();
        this.projectilesAlienTireurMoyenTouches = new ArrayList<ProjectileAlienTireurMoyen>();
        this.difficulteFacileChoisie = false;
        this.difficulteMoyenneChoisie = false;
        this.difficulteDifficileChoisie = false;
        this.projectilesAlienTireurDifficileTouches = new ArrayList<ProjectileAlienTireurDifficile>();
        this.aliensTireursDifficiles = new ArrayList<AlienTireurDifficile>();
        for (int i = 0; i < 1; ++i) {
            for (int j = 0; j < 4; ++j) {
                this.aliensTireursDifficiles.add(new AlienTireurDifficile(22 * j + 8, this.hauteur - (i + 2) * 6));
            }
        }
        this.aliensTireursDifficilesTouches = new ArrayList<AlienTireurDifficile>();
        this.aliensTireursDifficilesTouchesParLaser = new ArrayList<AlienTireurDifficile>();
        this.aliensDifficiles = new ArrayList<AlienDifficile>();
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 5; ++j) {
                this.aliensDifficiles.add(new AlienDifficile(18 * j + 5, this.hauteur - (i + 3) * 6));
            }
        }
        this.aliensDifficilesTouches = new ArrayList<AlienDifficile>();
        this.aliensDifficilesTouchesParLaser = new ArrayList<AlienDifficile>();
        this.projectilesAlienTireurDifficileTouches = new ArrayList<ProjectileAlienTireurDifficile>();
        this.projectilesDifficiles = new ArrayList<ProjectileDifficile>();
        this.projectilesDifficilesTouches = new ArrayList<ProjectileDifficile>();
        this.projectilesDifficilesRates = new ArrayList<ProjectileDifficile>();
        this.alienRapide = new ArrayList<AlienRapide>();
        this.alienRapide.add(new AlienRapide(5, this.hauteur - 29));
        this.alienRapideTouche = new ArrayList<AlienRapide>();
        this.alienRapideToucheParLaser = new ArrayList<AlienRapide>();
    }
    /** Permet de récupérer la hauteur de la fenêtre
     * @return la hauteur de la fenêtre
     */
    public int getHauteur() {
        return this.hauteur;
    }
    /** Permet de récupérer la largeur de la fenêtre
     * @return la largeur de la fenêtre
     */
    public int getLargeur() {
        return this.largeur;
    }
    /** permet de faire déplacer le vaisseau vers la gauche à l'aide de la flèche gauche du clavier */
    public void toucheGauche() {
        System.out.println("Appui sur la flèche gauche");
        if (this.vaisseau.getPosX() > 0) {
            this.vaisseau.deplace(-1.);
        }
    }
    /** permet de faire déplacer le vaisseau vers la droite à l'aide de la flèche droite du clavier */
    public void toucheDroite() {
        System.out.println("Appui sur la flèche droite");
        if (this.vaisseau.getPosX() < this.largeur - this.vaisseau.getEnsembleChaines().chaines.get(0).c.length()) {
            this.vaisseau.deplace(1.);
        }
    }
    /** permet de faire tirer le vaisseau à l'aide de la barre espace du clavier */
    public void toucheEspace() {
        System.out.println("Appui sur la touche espace");
        if (!this.isOver && ! this.estGagnee()) {
            if (this.difficulteFacileChoisie) { // si la difficulté facile est choisie
                if (this.peutTirer) {
                    this.projectiles.add(new Projectile(this.vaisseau.positionCanon(), 6)); // On ajoute des projectiles du niveau facile
                    for (Projectile p : this.projectiles) {
                        this.lesChaines.union(p.getEnsembleChaines());
                    }
                    this.peutTirer = false;
                }
            }
        }
        if (!this.isOver && !this.estGagneeM()) {
            if (this.difficulteMoyenneChoisie) { // si la difficulté moyenne est choisie
                if (this.peutTirer) {
                    this.projectilesMoyens.add(new ProjectileMoyen(this.vaisseau.positionCanon(), 6)); // On ajoute des projectiles du niveau moyen
                    for (ProjectileMoyen p : this.projectilesMoyens) {
                        this.lesChaines.union(p.getEnsembleChaines());
                    }
                    this.peutTirer = false;
                }
            }
        }
        if (!this.isOver && !this.estGagneeD()) {
            if (this.difficulteDifficileChoisie) {  // si la difficulté difficile est choisie
                if (this.peutTirer) {
                    this.projectilesDifficiles.add(new ProjectileDifficile(this.vaisseau.positionCanon(), 6)); // On ajoute des projectiles du niveau difficile
                    for (ProjectileDifficile p : this.projectilesDifficiles) {
                        this.lesChaines.union(p.getEnsembleChaines());
                    }
                    this.peutTirer = false;
                }
            }
        }
    }
    /** permet de tirer un laser avec le vaisseau à l'aide de la touche C du clavier */
    public void toucheC() {
        System.out.println("Appui sur la touche c");
        if (!this.isOver || !this.estGagnee() || !this.estGagneeM() || !this.estGagneeD()) {
            if (this.nbTirsLaser < 1) {
                this.lasers.add(new Laser(this.vaisseau.positionCanon(), 6));
                this.nbTirsLaser++;
                this.laserUtilise = true;
            }
        }
    }
    /** permet de choisir la difficulté facile à l'aide de la touche F du clavier */
    public void toucheF() {
        System.out.println("Difficulté facile");
        this.difficulteFacileChoisie = true;
        this.choixDifficulte = 1;
    }
    /** permet de choisir la difficulté moyenne à l'aide de la touche M du clavier */
    public void toucheM() {
        System.out.println("Difficulté moyenne");
        this.difficulteMoyenneChoisie = true;
        this.choixDifficulte = 2;
    }
    /** permet de choisir la difficulté difficile à l'aide de la touche D du clavier */
    public void toucheD() {
        System.out.println("Difficulté difficile");
        this.difficulteDifficileChoisie = true;
        this.choixDifficulte = 3;
    }
    /** permet de rejouer une partie à l'aide de la touche R du clavier */
    public void toucheR() {
        System.out.println("Rejouer");
        if (this.isOver || this.estGagnee() || this.estGagneeM() || this.estGagneeD()) {
            this.rejouer();
        }
    }
    /** permet de quitter le jeu à l'aide de la touche Q du clavier */
    public void toucheQ() {
        System.out.println("Quitter");
        if (this.isOver || this.estGagnee() || this.estGagneeM() || this.estGagneeD()) {
            this.quitter();
        }
    }
    /** Permet de récupérer le dessin du vaisseau
     * @return le dessin du vaisseau
     */
    public EnsembleChaines getChaines() {
        return this.lesChaines;
    }
    /** permet de définir si la partie au niveau facil est gagnée ou non
     * @return true si la partie est gagnée, false sinon
     */
    public boolean estGagnee() {
        return this.aliens.isEmpty() && this.aliensTireurs.isEmpty() && this.vie.getNbVies() > 0;
    }
    /** permet de définir si la partie au niveau moyen est gagnée ou non
     * @return true si la partie est gagnée, false sinon
     */
    public boolean estGagneeM() {
        return this.aliensMoyen.isEmpty() && this.aliensTireursMoyens.isEmpty() && this.vie.getNbVies() > 0;
    }
    /** permet de définir si la partie au niveau difficile est gagnée ou non
     * @return true si la partie est gagnée, false sinon
     */
    public boolean estGagneeD() {
        return this.aliensDifficiles.isEmpty() && this.aliensTireursDifficiles.isEmpty() && this.alienRapide.isEmpty() && this.vie.getNbVies() > 0;
    }
    /** permet de choisir la difficulté de la partie à jouer */
    public void choixDifficulte() {
        this.lesChaines.ajouteChaine(0, this.hauteur / 2 + 10," ██████╗██████╗  █████╗  █████╗ ███████╗  ██╗███╗  ██╗██╗   ██╗ █████╗ ██████╗ ███████╗██████╗  ██████╗");
        this.lesChaines.ajouteChaine(0, this.hauteur / 2 + 9, "██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔════╝  ██║████╗ ██║██║   ██║██╔══██╗██╔══██╗██╔════╝██╔══██╗██╔════╝");
        this.lesChaines.ajouteChaine(0, this.hauteur / 2 + 8, "╚█████╗ ██████╔╝███████║██║  ╚═╝█████╗    ██║██╔██╗██║╚██╗ ██╔╝███████║██║  ██║█████╗  ██████╔╝╚█████╗ ");
        this.lesChaines.ajouteChaine(0, this.hauteur / 2 + 7, " ╚═══██╗██╔═══╝ ██╔══██║██║  ██╗██╔══╝    ██║██║╚████║ ╚████╔╝ ██╔══██║██║  ██║██╔══╝  ██╔══██╗ ╚═══██╗");
        this.lesChaines.ajouteChaine(0, this.hauteur / 2 + 6, "██████╔╝██║     ██║  ██║╚█████╔╝███████╗  ██║██║ ╚███║  ╚██╔╝  ██║  ██║██████╔╝███████╗██║  ██║██████╔╝");
        this.lesChaines.ajouteChaine(0, this.hauteur / 2 + 5, "╚═════╝ ╚═╝     ╚═╝  ╚═╝ ╚════╝ ╚══════╝  ╚═╝╚═╝  ╚══╝   ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚══════╝╚═╝  ╚═╝╚═════╝ ");
        this.lesChaines.ajouteChaine(43, this.hauteur / 2, "Choisissez une difficulté :");
        this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 1, "F : facile");
        this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 2, "M : moyen");
        this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 3, "D : difficile");
    }
    /** permet de rejouer une partie à n'importe quelle difficulté */
    public void rejouer() {
        this.choixDifficulte = 0;
        this.difficulteFacileChoisie = false;
        this.difficulteDifficileChoisie = false;
        this.difficulteMoyenneChoisie = false;
        this.lesChaines = new EnsembleChaines();
        this.isOver = false;
        this.lesChaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(43);
        this.projectiles = new ArrayList<Projectile>();
        this.projectilesTouches = new ArrayList<Projectile>();
        this.projectilesRates = new ArrayList<Projectile>();
        this.score = new Score();
        this.isOver = false;
        this.peutTirer = true;
        this.aliens = new ArrayList<Alien>();
        for(int i = 0; i < 2; ++i){
            for(int j = 0; j < 5; ++j){
                this.aliens.add(new Alien(18 * j + 5, this.hauteur - (i + 3) * 6));
            }
        }
        this.aliensTouches = new ArrayList<Alien>(); 
        this.tempsDAttenteTir = 0;
        this.lasers = new ArrayList<Laser>();
        this.nbTirsLaser = 0;
        this.lasersTouches = new ArrayList<Laser>();
        this.lasersRates = new ArrayList<Laser>();
        this.aliensTouchesParLaser = new ArrayList<Alien>();
        this.aliensTireurs = new ArrayList<AlienTireur>();
        for (int i = 0; i < 1; ++i) {
            for (int j = 0; j < 4; ++j) {
                this.aliensTireurs.add(new AlienTireur(22 * j + 8, this.hauteur - (i + 2) * 6));
            }
        }
        this.aliensTireursTouches = new ArrayList<AlienTireur>();
        this.aliensTireursTouchesParLaser = new ArrayList<AlienTireur>();
        this.projectilesAlienTireurTouches = new ArrayList<ProjectileAlienTireur>();
        this.laserUtilise = false;
        this.vie = new Vie(2);
        this.tempsDAttenteVaisseauBonus1 = 0;
        this.tempsDAttenteVaisseauBonus2 = 0;
        this.lancerVB1 = false;
        this.lancerVB2 = false;
        this.vaisseauxBonus = new ArrayList<VaisseauBonus>();
        this.vaisseauxBonus.add(new VaisseauBonus(this.largeur + 1, this.hauteur - 5));
        this.vaisseauxBonus.add(new VaisseauBonus(this.largeur + 1, this.hauteur - 13));
        this.vaisseauxBonusTouches = new ArrayList<VaisseauBonus>();
        this.vaisseauxBonusTouchesParLaser = new ArrayList<VaisseauBonus>();
        this.choixDifficulte = 0;
        this.projectilesMoyens = new ArrayList<ProjectileMoyen>();
        this.projectilesMoyensTouches = new ArrayList<ProjectileMoyen>();
        this.projectilesMoyensRates = new ArrayList<ProjectileMoyen>();
        this.aliensMoyen = new ArrayList<AlienMoyen>();
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 5; ++j) {
                this.aliensMoyen.add(new AlienMoyen(18 * j + 5, this.hauteur - (i + 3) * 6));
            }
        }
        this.aliensMoyenTouches = new ArrayList<AlienMoyen>();
        this.aliensMoyensTouchesParLaser = new ArrayList<AlienMoyen>();
        this.aliensTireursMoyens = new ArrayList<AlienTireurMoyen>();
        for (int i = 0; i < 1; ++i) {
            for (int j = 0; j < 4; ++j) {
                this.aliensTireursMoyens.add(new AlienTireurMoyen(22 * j + 8, this.hauteur - (i + 2) * 6));
            }
        }
        this.aliensTireursMoyensTouches = new ArrayList<AlienTireurMoyen>();
        this.aliensTireursMoyensTouchesParLaser = new ArrayList<AlienTireurMoyen>();
        this.projectilesAlienTireurMoyenTouches = new ArrayList<ProjectileAlienTireurMoyen>();
        this.difficulteFacileChoisie = false;
        this.difficulteMoyenneChoisie = false;
        this.difficulteDifficileChoisie = false;
        this.projectilesAlienTireurDifficileTouches = new ArrayList<ProjectileAlienTireurDifficile>();
        this.aliensTireursDifficiles = new ArrayList<AlienTireurDifficile>();
        for (int i = 0; i < 1; ++i) {
            for (int j = 0; j < 4; ++j) {
                this.aliensTireursDifficiles.add(new AlienTireurDifficile(22 * j + 8, this.hauteur - (i + 2) * 6));
            }
        }
        this.aliensTireursDifficilesTouches = new ArrayList<AlienTireurDifficile>();
        this.aliensTireursDifficilesTouchesParLaser = new ArrayList<AlienTireurDifficile>();
        this.aliensDifficiles = new ArrayList<AlienDifficile>();
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 5; ++j) {
                this.aliensDifficiles.add(new AlienDifficile(18 * j + 5, this.hauteur - (i + 3) * 6));
            }
        }
        this.aliensDifficilesTouches = new ArrayList<AlienDifficile>();
        this.aliensDifficilesTouchesParLaser = new ArrayList<AlienDifficile>();
        this.projectilesAlienTireurDifficileTouches = new ArrayList<ProjectileAlienTireurDifficile>();
        this.projectilesDifficiles = new ArrayList<ProjectileDifficile>();
        this.projectilesDifficilesTouches = new ArrayList<ProjectileDifficile>();
        this.projectilesDifficilesRates = new ArrayList<ProjectileDifficile>();
        this.alienRapide = new ArrayList<AlienRapide>();
        this.alienRapide.add(new AlienRapide(5, this.hauteur - 29));
        this.alienRapideTouche = new ArrayList<AlienRapide>();
        this.alienRapideToucheParLaser = new ArrayList<AlienRapide>();
    }
    /** permet de quitter le jeu si on se souhaite pas rejouer une partie */
    public void quitter() {
        this.lesChaines = new EnsembleChaines();
        this.choixDifficulte = -1;
    }
    /** permet de jouer un tour au niveau facile */
    public void niveauFacile() {
        this.lesChaines = new EnsembleChaines();
        if (this.estGagnee()) {
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 6, "██╗   ██╗ █████╗ ██╗   ██╗   ██╗       ██╗ █████╗ ███╗  ██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 5, "╚██╗ ██╔╝██╔══██╗██║   ██║   ██║  ██╗  ██║██╔══██╗████╗ ██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 4, " ╚████╔╝ ██║  ██║██║   ██║   ╚██╗████╗██╔╝██║  ██║██╔██╗██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 3, "  ╚██╔╝  ██║  ██║██║   ██║    ████╔═████║ ██║  ██║██║╚████║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 2, "   █║    ╚█████╔╝╚██████╔╝    ╚██╔╝ ╚██╔╝ ╚█████╔╝██║ ╚███║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 1, "  ╚═╝     ╚════╝  ╚═════╝      ╚═╝   ╚═╝   ╚════╝ ╚═╝  ╚══╝");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 1, "Votre score : " + this.score.getScore());
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 3, "R : rejouer");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 4, "Q : quitter");
            return;
        } 
        if (this.isOver) {
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 6, " ██████╗  █████╗ ███╗   ███╗███████╗   █████╗ ██╗   ██╗███████╗██████╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 5, "██╔════╝ ██╔══██╗████╗ ████║██╔════╝  ██╔══██╗██║   ██║██╔════╝██╔══██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 4, "██║  ██╗ ███████║██╔████╔██║█████╗    ██║  ██║╚██╗ ██╔╝█████╗  ██████╔╝");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 3, "██║  ╚██╗██╔══██║██║╚██╔╝██║██╔══╝    ██║  ██║ ╚████╔╝ ██╔══╝  ██╔══██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 2, "╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗  ╚█████╔╝  ╚██╔╝  ███████╗██║  ██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 1, " ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝   ╚════╝    ╚═╝   ╚══════╝╚═╝  ╚═╝");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 1, "Votre score : " + this.score.getScore());
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 2, "Voulez-vous rejouer ?");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 3, "R : rejouer");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 4, "Q : quitter");
            return;
        }
        for (Projectile p : this.projectiles) {
            if (p.getPosY() <= this.hauteur + 1 && !this.isOver && !this.estGagnee()) {
                p.evolue();
            }
            else {
                this.projectilesRates.add(p);
                if (!this.isOver || !this.estGagnee()) { this.score.ajoute(-20); }
                this.peutTirer = true;
            }
            for (Alien a : this.aliens) {
                if (a.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.aliensTouches.add(a);
                    this.projectilesTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver ||  !this.estGagnee()) { this.score.ajoute(10); }
                    break;
                }
            }
            for (AlienTireur a : this.aliensTireurs) {
                if (a.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.aliensTireursTouches.add(a);
                    this.projectilesTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver ||  !this.estGagnee()) { this.score.ajoute(30); }
                    break;
                }
            }
            for (VaisseauBonus v : this.vaisseauxBonus) {
                if (v.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.vaisseauxBonusTouches.add(v);
                    this.projectilesTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver ||  !this.estGagnee()) { this.score.ajoute(100); }
                    break;
                }
            }
            this.lesChaines.union(p.getEnsembleChaines());         
        }
        for (Laser l : this.lasers) {
            if (l.getPosY() <= this.hauteur &&!this.isOver && !this.estGagnee()) {
                l.evolue();
            }
            for (Alien a : this.aliens) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (a.contient((int) c.x, (int) c.y)) {
                        this.aliensTouchesParLaser.add(a);
                        this.lasersTouches.add(l);
                        if (!this.isOver || !this.estGagnee()) { this.score.ajoute(10); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            for (AlienTireur a : this.aliensTireurs) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (a.contient((int) c.x, (int) c.y)) {
                        this.aliensTireursTouchesParLaser.add(a);
                        this.lasersTouches.add(l);
                        if (!this.isOver || !this.estGagnee()) { this.score.ajoute(30); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            for (VaisseauBonus v : this.vaisseauxBonus) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (v.contient((int) c.x, (int) c.y)) {
                        this.vaisseauxBonusTouchesParLaser.add(v);
                        this.lasersTouches.add(l);
                        if (!this.isOver || !this.estGagnee()) { this.score.ajoute(100); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            this.lesChaines.union(l.getEnsembleChaines());
        }
        for (Alien a : this.aliens) {
            if (a.getPositionY() == 10) {
                this.isOver = true;
            }
            if (!this.isOver) {
                a.evolue();
                this.lesChaines.union(a.getEnsembleChaines());
            }
            
        }
        for (Alien a : this.aliensTouches) {
            this.aliens.remove(a);
        }
        for (AlienTireur a : this.aliensTireurs) {
            if (a.getPositionY() == 10) {
                this.isOver = true;
            }
            if (!this.isOver) {
                a.evolue();
                this.lesChaines.union(a.getEnsembleChaines());
            }
        }       
        for (AlienTireur a : this.aliensTireurs) {
            for (ProjectileAlienTireur p : a.getProjectilesAliensTireurs()) {
                if (this.vaisseau.contient((int) p.getPosX(), (int) p.getPosY())) {
                    if (this.projectilesAlienTireurTouches.size() < 1) { 
                        this.projectilesAlienTireurTouches.add(p);
                        this.vie.setNbVies(1);
                    }
                    if (this.projectilesAlienTireurTouches.size() < 2 && !this.projectilesAlienTireurTouches.contains(p)) {
                        this.projectilesAlienTireurTouches.add(p);
                        this.vie.setNbVies(0);
                    }
                    if (this.projectilesAlienTireurTouches.size() == 2) {
                        this.isOver = true;
                    }                    
                }
                if (!this.isOver) { this.lesChaines.union(p.getEnsembleChaines()); }
            }
            for (ProjectileAlienTireur p : a.getProjectilesAliensTireursRates()) {
                a.getProjectilesAliensTireurs().remove(p);
            }
        }
        for (Projectile p : this.projectilesRates) {
            this.projectiles.remove(p);
        }
        for (Projectile p : this.projectilesTouches) {
            this.projectiles.remove(p);
        }
        if (!this.peutTirer) {
            this.tempsDAttenteTir ++;
        }
        if (this.tempsDAttenteTir == 30) { // 30 correspond au temps d'attente avant le prochain tir
            this.peutTirer = true;
            this.tempsDAttenteTir = 0;
        }
        for (Laser l : this.lasersRates) {
            this.lasers.remove(l);
        }
        for (Laser l : this.lasersTouches) {
            this.lasers.remove(l);        
        }
        for (Alien a : this.aliensTouchesParLaser) {
            this.aliens.remove(a);
        }
        for (AlienTireur a : this.aliensTireursTouches) {
            this.aliensTireurs.remove(a);
        }
        for (AlienTireur a : this.aliensTireursTouchesParLaser) {
            this.aliensTireurs.remove(a);
        }
        for (ProjectileAlienTireur p : this.projectilesAlienTireurTouches) {
            for (AlienTireur a : this.aliensTireurs) {
                a.getProjectilesAliensTireurs().remove(p);
            }
        }
        if (!this.lancerVB1) {
            this.tempsDAttenteVaisseauBonus1 ++;
        }
        for (int i = 0; i < this.vaisseauxBonus.size(); ++i) {
            if (i == 0) {
                if (this.tempsDAttenteVaisseauBonus1 == 500) {
                    this.lancerVB1 = true;
                    if (!this.isOver && this.vaisseauxBonus.get(i).getPositionX() > -18 && !this.estGagnee()) {
                        this.vaisseauxBonus.get(i).evolue();
                    } 
                }
            }
            if (i == 1) {
                if (!this.lancerVB2) {
                    this.tempsDAttenteVaisseauBonus2 ++;
                }
                if (this.tempsDAttenteVaisseauBonus2 == 1300) {
                    this.lancerVB2 = true;
                    if (!this.isOver && this.vaisseauxBonus.get(i).getPositionX() > -18 && !this.estGagnee()) {
                        this.vaisseauxBonus.get(i).evolue();
                    } 
                }
            }     
            if (!this.estGagnee()) this.lesChaines.union(this.vaisseauxBonus.get(i).getEnsembleChaines());             
        }
        for (VaisseauBonus v : this.vaisseauxBonusTouches) {
            this.vaisseauxBonus.remove(v);
        }
        for (VaisseauBonus v : this.vaisseauxBonusTouchesParLaser) {
            this.vaisseauxBonus.remove(v);
        }
        this.lesChaines.union(this.vaisseau.getEnsembleChaines());    
        if (!this.isOver && !this.estGagnee()) {
            this.lesChaines.ajouteChaine(0, this.hauteur - 1, "SCORE : " + this.score.getScore());
        }
        if (!this.isOver && !this.estGagnee()) {
            if (!this.laserUtilise) {
                this.lesChaines.ajouteChaine(0, this.hauteur - 2, "NOMBRE DE LASERS : 1");
            }
            else {
                this.lesChaines.ajouteChaine(0, this.hauteur - 2, "NOMBRE DE LASERS : 0");
            }
        }
        if (!this.isOver && !this.estGagnee()) {
            this.lesChaines.ajouteChaine(25, this.hauteur - 1, "NOMBRE DE VIES : " + this.vie.getNbVies());
        }      
        if (!this.isOver || !this.estGagnee()) {
            this.score.ajoute(1);
        }
    }
    /** permet de jouer un tour au niveau moyen */
    public void niveauMoyen() {
        this.lesChaines = new EnsembleChaines();
        if (this.estGagnee()) {
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 6, "██╗   ██╗ █████╗ ██╗   ██╗   ██╗       ██╗ █████╗ ███╗  ██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 5, "╚██╗ ██╔╝██╔══██╗██║   ██║   ██║  ██╗  ██║██╔══██╗████╗ ██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 4, " ╚████╔╝ ██║  ██║██║   ██║   ╚██╗████╗██╔╝██║  ██║██╔██╗██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 3, "  ╚██╔╝  ██║  ██║██║   ██║    ████╔═████║ ██║  ██║██║╚████║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 2, "   █║    ╚█████╔╝╚██████╔╝    ╚██╔╝ ╚██╔╝ ╚█████╔╝██║ ╚███║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 1, "  ╚═╝     ╚════╝  ╚═════╝      ╚═╝   ╚═╝   ╚════╝ ╚═╝  ╚══╝");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 1, "Votre score : " + this.score.getScore());
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 3, "R : rejouer");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 4, "Q : quitter");
            return;
        } 
        if (this.isOver) {
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 6, " ██████╗  █████╗ ███╗   ███╗███████╗   █████╗ ██╗   ██╗███████╗██████╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 5, "██╔════╝ ██╔══██╗████╗ ████║██╔════╝  ██╔══██╗██║   ██║██╔════╝██╔══██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 4, "██║  ██╗ ███████║██╔████╔██║█████╗    ██║  ██║╚██╗ ██╔╝█████╗  ██████╔╝");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 3, "██║  ╚██╗██╔══██║██║╚██╔╝██║██╔══╝    ██║  ██║ ╚████╔╝ ██╔══╝  ██╔══██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 2, "╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗  ╚█████╔╝  ╚██╔╝  ███████╗██║  ██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 1, " ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝   ╚════╝    ╚═╝   ╚══════╝╚═╝  ╚═╝");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 1, "Votre score : " + this.score.getScore());
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 2, "Voulez-vous rejouer ?");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 3, "R : rejouer");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 4, "Q : quitter");
            return;
        }
        for (ProjectileMoyen p : this.projectilesMoyens) {
            if (p.getPosY() <= this.hauteur + 1 && !this.isOver && !this.estGagneeM()) {
                p.evolue();
            }
            else {
                this.projectilesMoyensRates.add(p);
                if (!this.isOver || this.estGagneeM()) { this.score.ajoute(-20); }
                this.peutTirer = true;
            }
            for (AlienMoyen a : this.aliensMoyen) {
                if (a.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.aliensMoyenTouches.add(a);
                    this.projectilesMoyensTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver || this.estGagneeM()) { this.score.ajoute(10); }
                    break;
                }
            }
            for (AlienTireurMoyen a : this.aliensTireursMoyens) {
                if (a.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.aliensTireursMoyensTouches.add(a);
                    this.projectilesMoyensTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver || this.estGagneeM()) { this.score.ajoute(30); }
                    break;
                }
            }
            for (VaisseauBonus v : this.vaisseauxBonus) {
                if (v.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.vaisseauxBonusTouches.add(v);
                    this.projectilesMoyensTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver || this.estGagneeM()) { this.score.ajoute(100); }
                    break;
                }
            }
            this.lesChaines.union(p.getEnsembleChaines());         
        }
        for (Laser l : this.lasers) {
            if (l.getPosY() <= this.hauteur && !this.isOver &&!this.estGagneeM()) {
            l.evolue();
            }
            for (AlienMoyen a : this.aliensMoyen) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (a.contient((int) c.x, (int) c.y)) {
                        this.aliensMoyensTouchesParLaser.add(a);
                        this.lasersTouches.add(l);
                        if (!this.isOver || this.estGagneeM()) { this.score.ajoute(10); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            for (AlienTireurMoyen a : this.aliensTireursMoyens) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (a.contient((int) c.x, (int) c.y)) {
                        this.aliensTireursMoyensTouchesParLaser.add(a);
                        this.lasersTouches.add(l);
                        if (!this.isOver || this.estGagneeM()) { this.score.ajoute(30); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            for (VaisseauBonus v : this.vaisseauxBonus) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (v.contient((int) c.x, (int) c.y)) {
                        this.vaisseauxBonusTouchesParLaser.add(v);
                        this.lasersTouches.add(l);
                        if (!this.isOver || this.estGagneeM()) { this.score.ajoute(100); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            this.lesChaines.union(l.getEnsembleChaines());
        }
        for (AlienMoyen a : this.aliensMoyen) {
            if (a.getPositionY() == 10) {
                this.isOver = true;
            }
            if (!this.isOver) {
                a.evolue();
                this.lesChaines.union(a.getEnsembleChaines());
            }
            
        }
        for (AlienMoyen a : this.aliensMoyenTouches) {
            this.aliensMoyen.remove(a);
        }
        for (AlienTireurMoyen a : this.aliensTireursMoyens) {
            if (a.getPositionY() == 10) {
                this.isOver = true;
            }
            if (!this.isOver) {
                a.evolue();
                this.lesChaines.union(a.getEnsembleChaines());
            }
        }       
        for (AlienTireurMoyen a : this.aliensTireursMoyens) {
            for (ProjectileAlienTireurMoyen p : a.getProjectilesAliensTireursMoyens()) {
                if (this.vaisseau.contient((int) p.getPosX(), (int) p.getPosY())) {
                    if (this.projectilesAlienTireurMoyenTouches.size() < 1) { 
                        this.projectilesAlienTireurMoyenTouches.add(p);
                        this.vie.setNbVies(1);
                    }
                    if (this.projectilesAlienTireurMoyenTouches.size() < 2 && !this.projectilesAlienTireurMoyenTouches.contains(p)) {
                        this.projectilesAlienTireurMoyenTouches.add(p);
                        this.vie.setNbVies(0);
                    }
                    if (this.projectilesAlienTireurMoyenTouches.size() == 2) {
                        this.isOver = true;
                    }                    
                }
                if (!this.isOver) { this.lesChaines.union(p.getEnsembleChaines()); }
            }
            for (ProjectileAlienTireurMoyen p : a.getProjectilesAliensTireursRatesMoyens()) {
                a.getProjectilesAliensTireursMoyens().remove(p);
            }
        }
        for (ProjectileMoyen p : this.projectilesMoyensRates) {
            this.projectilesMoyens.remove(p);
        }
        for (ProjectileMoyen p : this.projectilesMoyensTouches) {
            this.projectilesMoyens.remove(p);
        }
        if (!this.peutTirer) {
            this.tempsDAttenteTir ++;
        }
        if (this.tempsDAttenteTir == 30) { // 30 correspond au temps d'attente avant le prochain tir
            this.peutTirer = true;
            this.tempsDAttenteTir = 0;
        }
        for (Laser l : this.lasersRates) {
            this.lasers.remove(l);
        }
        for (Laser l : this.lasersTouches) {
            this.lasers.remove(l);        
        }
        for (AlienMoyen a : this.aliensMoyensTouchesParLaser) {
            this.aliensMoyen.remove(a);
        }
        for (AlienTireurMoyen a : this.aliensTireursMoyensTouches) {
            this.aliensTireursMoyens.remove(a);
        }
        for (AlienTireurMoyen a : this.aliensTireursMoyensTouchesParLaser) {
            this.aliensTireursMoyens.remove(a);
        }
        for (ProjectileAlienTireurMoyen p : this.projectilesAlienTireurMoyenTouches) {
            for (AlienTireurMoyen a : this.aliensTireursMoyens) {
                a.getProjectilesAliensTireursMoyens().remove(p);
            }
        }
        if (!this.lancerVB1) {
            this.tempsDAttenteVaisseauBonus1 ++;
        }
        for (int i = 0; i < this.vaisseauxBonus.size(); ++i) {
            if (i == 0) {
                if (this.tempsDAttenteVaisseauBonus1 == 500) {
                    this.lancerVB1 = true;
                    if (!this.isOver && this.vaisseauxBonus.get(i).getPositionX() > -18 && !this.estGagneeM()) {
                        this.vaisseauxBonus.get(i).evolue();
                    } 
                }
            }
            if (i == 1) {
                if (!this.lancerVB2) {
                    this.tempsDAttenteVaisseauBonus2 ++;
                }
                if (this.tempsDAttenteVaisseauBonus2 == 1300) {
                    this.lancerVB2 = true;
                    if (!this.isOver && this.vaisseauxBonus.get(i).getPositionX() > -18 && !this.estGagneeM()) {
                    this.vaisseauxBonus.get(i).evolue();
                    } 
                }
            }     
            if (!this.estGagneeM()) this.lesChaines.union(this.vaisseauxBonus.get(i).getEnsembleChaines());             
        }
        for (VaisseauBonus v : this.vaisseauxBonusTouches) {
            this.vaisseauxBonus.remove(v);
        }
        for (VaisseauBonus v : this.vaisseauxBonusTouchesParLaser) {
            this.vaisseauxBonus.remove(v);
        }
        this.lesChaines.union(this.vaisseau.getEnsembleChaines());    
        if (!this.isOver && !this.estGagneeM()) {
            this.lesChaines.ajouteChaine(0, this.hauteur - 1, "SCORE : " + this.score.getScore());
        }
        if (!this.isOver && !this.estGagneeM()) {
            if (!this.laserUtilise) {
                this.lesChaines.ajouteChaine(0, this.hauteur - 2, "NOMBRE DE LASERS : 1");
            }
            else {
                this.lesChaines.ajouteChaine(0, this.hauteur - 2, "NOMBRE DE LASERS : 0");
            }
        }
        if (!this.isOver && !this.estGagneeM()) {
            this.lesChaines.ajouteChaine(25, this.hauteur - 1, "NOMBRE DE VIES : " + this.vie.getNbVies());
        }      
        if (!this.isOver || !this.estGagneeM()) {
            this.score.ajoute(1);
        }
    }
    /** permet de jouer un tour au niveau difficle */
    public void niveauDifficile() {
        this.lesChaines = new EnsembleChaines();
        if (this.estGagnee()) {
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 6, "██╗   ██╗ █████╗ ██╗   ██╗   ██╗       ██╗ █████╗ ███╗  ██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 5, "╚██╗ ██╔╝██╔══██╗██║   ██║   ██║  ██╗  ██║██╔══██╗████╗ ██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 4, " ╚████╔╝ ██║  ██║██║   ██║   ╚██╗████╗██╔╝██║  ██║██╔██╗██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 3, "  ╚██╔╝  ██║  ██║██║   ██║    ████╔═████║ ██║  ██║██║╚████║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 2, "   █║    ╚█████╔╝╚██████╔╝    ╚██╔╝ ╚██╔╝ ╚█████╔╝██║ ╚███║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 1, "  ╚═╝     ╚════╝  ╚═════╝      ╚═╝   ╚═╝   ╚════╝ ╚═╝  ╚══╝");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 1, "Votre score : " + this.score.getScore());
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 3, "R : rejouer");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 4, "Q : quitter");
            return;
        } 
        if (this.isOver) {
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 6, " ██████╗  █████╗ ███╗   ███╗███████╗   █████╗ ██╗   ██╗███████╗██████╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 5, "██╔════╝ ██╔══██╗████╗ ████║██╔════╝  ██╔══██╗██║   ██║██╔════╝██╔══██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 4, "██║  ██╗ ███████║██╔████╔██║█████╗    ██║  ██║╚██╗ ██╔╝█████╗  ██████╔╝");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 3, "██║  ╚██╗██╔══██║██║╚██╔╝██║██╔══╝    ██║  ██║ ╚████╔╝ ██╔══╝  ██╔══██╗");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 2, "╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗  ╚█████╔╝  ╚██╔╝  ███████╗██║  ██║");
            this.lesChaines.ajouteChaine(20, this.hauteur / 2 + 1, " ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝   ╚════╝    ╚═╝   ╚══════╝╚═╝  ╚═╝");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 1, "Votre score : " + this.score.getScore());
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 2, "Voulez-vous rejouer ?");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 3, "R : rejouer");
            this.lesChaines.ajouteChaine(43, this.hauteur / 2 - 4, "Q : quitter");
            return;
        }
        for (ProjectileDifficile p : this.projectilesDifficiles) {
            if (p.getPosY() <= this.hauteur + 1 && !this.isOver && !this.estGagneeD()) {
                p.evolue();
            }
            else {
                this.projectilesDifficilesRates.add(p);
                if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(-20); }
                this.peutTirer = true;
            }
            for (AlienDifficile a : this.aliensDifficiles) {
                if (a.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.aliensDifficilesTouches.add(a);
                    this.projectilesDifficilesTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(10); }
                    break;
                }
            }
            for (AlienTireurDifficile a : this.aliensTireursDifficiles) {
                if (a.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.aliensTireursDifficilesTouches.add(a);
                    this.projectilesDifficilesTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(30); }
                    break;
                }
            }
            for (VaisseauBonus v : this.vaisseauxBonus) {
                if (v.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.vaisseauxBonusTouches.add(v);
                    this.projectilesDifficilesTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(100); }
                    break;
                }
            }
            for (AlienRapide a : this.alienRapide) {
                if (a.contient((int) p.getPosX(), (int) p.getPosY())) {
                    this.alienRapideTouche.add(a);
                    this.projectilesDifficilesTouches.add(p);
                    this.peutTirer = true;
                    if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(20); }
                    break;
                }
            }
            this.lesChaines.union(p.getEnsembleChaines());         
        }
        for (Laser l : this.lasers) {
            if (l.getPosY() <= this.hauteur &&!this.isOver) {
                l.evolue();
            }
            for (AlienDifficile a : this.aliensDifficiles) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (a.contient((int) c.x, (int) c.y)) {
                        this.aliensDifficilesTouchesParLaser.add(a);
                        this.lasersTouches.add(l);
                        if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(10); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            for (AlienTireurDifficile a : this.aliensTireursDifficiles) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (a.contient((int) c.x, (int) c.y)) {
                        this.aliensTireursDifficilesTouchesParLaser.add(a);
                        this.lasersTouches.add(l);
                        if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(30); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            for (VaisseauBonus v : this.vaisseauxBonus) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (v.contient((int) c.x, (int) c.y)) {
                        this.vaisseauxBonusTouchesParLaser.add(v);
                        this.lasersTouches.add(l);
                        if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(100); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            for (AlienRapide a : this.alienRapide) {
                for (ChainePositionnee c : l.getEnsembleChaines().chaines) {
                    if (a.contient((int) c.x, (int) c.y)) {
                        this.alienRapideToucheParLaser.add(a);
                        this.lasersTouches.add(l);
                        if (!this.isOver || !this.estGagneeD()) { this.score.ajoute(20); }
                        break;   
                    }
                    else {
                        this.lasersRates.add(l);
                    }
                }
            }
            this.lesChaines.union(l.getEnsembleChaines());
        }
        for (AlienDifficile a : this.aliensDifficiles) {
            if (a.getPositionY() == 10) {
                this.isOver = true;
            }
            if (!this.isOver) {
                a.evolue();
                this.lesChaines.union(a.getEnsembleChaines());
            }            
        }
        for (AlienDifficile a : this.aliensDifficilesTouches) {
            this.aliensDifficiles.remove(a);
        }
        for (AlienTireurDifficile a : this.aliensTireursDifficiles) {
            if (a.getPositionY() == 10) {
                this.isOver = true;
            }
            if (!this.isOver) {
                a.evolue();
                this.lesChaines.union(a.getEnsembleChaines());
            }
        }       
        for (AlienTireurDifficile a : this.aliensTireursDifficiles) {
            for (ProjectileAlienTireurDifficile p : a.getProjectilesAliensTireursDifficiles()) {
                if (this.vaisseau.contient((int) p.getPosX(), (int) p.getPosY())) {
                    if (this.projectilesAlienTireurDifficileTouches.size() < 1) { 
                        this.projectilesAlienTireurDifficileTouches.add(p);
                        this.vie.setNbVies(1);
                    }
                    if (this.projectilesAlienTireurDifficileTouches.size() < 2 && !this.projectilesAlienTireurDifficileTouches.contains(p)) {
                        this.projectilesAlienTireurDifficileTouches.add(p);
                        this.vie.setNbVies(0);
                    }
                    if (this.projectilesAlienTireurDifficileTouches.size() == 2) {
                        this.isOver = true;
                    }                    
                }
                if (!this.isOver) { this.lesChaines.union(p.getEnsembleChaines()); }
            }
            for (ProjectileAlienTireurDifficile p : a.getProjectilesAliensTireursRatesDifficiles()) {
                a.getProjectilesAliensTireursDifficiles().remove(p);
            }
        }
        for (ProjectileDifficile p : this.projectilesDifficilesRates) {
            this.projectilesDifficiles.remove(p);
        }
        for (ProjectileDifficile p : this.projectilesDifficilesTouches) {
            this.projectilesDifficiles.remove(p);
        }
        if (!this.peutTirer) {
            this.tempsDAttenteTir ++;
        }
        if (this.tempsDAttenteTir == 30) { // 30 correspond au temps d'attente avant le prochain tir
            this.peutTirer = true;
            this.tempsDAttenteTir = 0;
        }
        for (Laser l : this.lasersRates) {
            this.lasers.remove(l);
        }
        for (Laser l : this.lasersTouches) {
            this.lasers.remove(l);        
        }
        for (AlienDifficile a : this.aliensDifficilesTouchesParLaser) {
            this.aliensDifficiles.remove(a);
        }
        for (AlienTireurDifficile a : this.aliensTireursDifficilesTouches) {
            this.aliensTireursDifficiles.remove(a);
        }
        for (AlienTireurDifficile a : this.aliensTireursDifficilesTouchesParLaser) {
            this.aliensTireursDifficiles.remove(a);
        }
        for (ProjectileAlienTireurDifficile p : this.projectilesAlienTireurDifficileTouches) {
            for (AlienTireurDifficile a : this.aliensTireursDifficiles) {
                a.getProjectilesAliensTireursDifficiles().remove(p);
            }
        }
        if (!this.lancerVB1) {
            this.tempsDAttenteVaisseauBonus1 ++;
        }
        for (int i = 0; i < this.vaisseauxBonus.size(); ++i) {
            if (i == 0) {
                if (this.tempsDAttenteVaisseauBonus1 == 500) {
                    this.lancerVB1 = true;
                    if (!this.isOver && this.vaisseauxBonus.get(i).getPositionX() > -18 && !this.estGagneeD()) {
                        this.vaisseauxBonus.get(i).evolue();
                    } 
                }
            }
            if (i == 1) {
                if (!this.lancerVB2) {
                    this.tempsDAttenteVaisseauBonus2 ++;
                }
                if (this.tempsDAttenteVaisseauBonus2 == 1300) {
                    this.lancerVB2 = true;
                    if (!this.isOver && this.vaisseauxBonus.get(i).getPositionX() > -18 && !this.estGagneeD()) {
                    this.vaisseauxBonus.get(i).evolue();
                    } 
                }
            }     
            if (!this.estGagneeD()) this.lesChaines.union(this.vaisseauxBonus.get(i).getEnsembleChaines());             
        }
        for (VaisseauBonus v : this.vaisseauxBonusTouches) {
            this.vaisseauxBonus.remove(v);
        }
        for (VaisseauBonus v : this.vaisseauxBonusTouchesParLaser) {
            this.vaisseauxBonus.remove(v);
        }
        for (AlienRapide a : this.alienRapide) {
            if (a.getPositionY() == 10) {
                this.isOver = true;
            }
            if (!this.isOver) {
                a.evolue();
                this.lesChaines.union(a.getEnsembleChaines());
            }
        }
        for (AlienRapide a : this.alienRapideTouche) {
            this.alienRapide.remove(a);
        }
        for (AlienRapide a : this.alienRapideToucheParLaser) {
            this.alienRapide.remove(a);
        }
        this.lesChaines.union(this.vaisseau.getEnsembleChaines());    
        if (!this.isOver && !this.estGagneeD()) {
            this.lesChaines.ajouteChaine(0, this.hauteur - 1, "SCORE : " + this.score.getScore());
        }
        if (!this.isOver && !this.estGagneeD()) {
            if (!this.laserUtilise) {
                this.lesChaines.ajouteChaine(0, this.hauteur - 2, "NOMBRE DE LASERS : 1");
            }
            else {
                this.lesChaines.ajouteChaine(0, this.hauteur - 2, "NOMBRE DE LASERS : 0");
            }
        }
        if (!this.isOver && !this.estGagneeD()) {
            this.lesChaines.ajouteChaine(25, this.hauteur - 1, "NOMBRE DE VIES : " + this.vie.getNbVies());
        }      
        if (!this.isOver || !this.estGagneeD()) {
            this.score.ajoute(1);
        }
    }
    /** permet de jouer une partie en choisissant la difficulté */
    public void jouerUnTour() {
        switch (this.choixDifficulte) {
            case 0 :
                this.choixDifficulte();
                break;
            case 1 : 
                this.niveauFacile(); 
                break;
            case 2 :
                this.niveauMoyen();
                break;
            case 3:
                this.niveauDifficile();
                break;
            case -1:
                this.lesChaines.ajouteChaine(43, this.hauteur / 2, "D'accord, à bientôt !");
                break;
        }
    }
}
