public class Laser {
    /** Cet attribut modélise la position du laser concernant la hauteur de la fenêtre */
    private double positionX;
    /** Cet attribut modélise la position du laser concernant la largeur de la fenêtre */
    private double positionY;
    /** Cet attribut modélise le dessin du laser */
    private EnsembleChaines dessin;
    /** Permet de construire le laser
     * @param positionX la position du laser concernant la hauteur de la fenêtre
     * @param positionY la position du laser concernant la largeur de la fenêtre
     */
    public Laser(double positionX, double positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.dessin = new EnsembleChaines(); 
    }
    /** Permet de récupérer la position du laser concernant la hauteur de la fenêtre
     * @return la position du laser concernant la hauteur de la fenêtre
     */
    public double getPosX() {
        return this.positionX;
    }
    /** Permet de récupérer la position du laser concernant la largeur de la fenêtre
     * @return la position du laser concernant la largeur de la fenêtre
     */
    public double getPosY() {
        return this.positionY;
    }
    /** Permet de récupérer le dessin du laser
     * @return le dessin du laser
     */
    public EnsembleChaines getEnsembleChaines() {
        this.dessin = new EnsembleChaines();
        for (int i = 0; i < 54; ++i) {
            this.dessin.ajouteChaine((int) positionX, (int) positionY + i, "█");  
        }  
        return this.dessin;
    }
    /** Permet de faire évoluer le laser */
    public void evolue() {
        this.positionY = 6;
        for (ChainePositionnee c : this.dessin.chaines) {
            c.y = (int) this.positionY;
        }
    }
}
