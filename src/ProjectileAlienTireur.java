public class ProjectileAlienTireur {
    /** Cet attribut modélise la position du projectile concernant la hauteur de la fenêtre */
    private double positionX;
    /** Cet attribut modélise la position du projectile concernant la largeur de la fenêtre */
    private double positionY;
    /** Cet attribut modélise le dessin du projectile */
    private EnsembleChaines dessin;
    /** Permet de construire le projectile
     * @param positionX la position du projectile concernant la hauteur de la fenêtre
     * @param positionY la position du projectile concernant la largeur de la fenêtre
     */
    public ProjectileAlienTireur(double positionX, double positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.dessin = new EnsembleChaines();  
    }
    /** Permet de récupérer la position du projectile concernant la hauteur de la fenêtre
     * @return la position du projectile concernant la hauteur de la fenêtre
     */
    public double getPosX() {
        return this.positionX;
    }
    /** Permet de récupérer la position du projectile concernant la largeur de la fenêtre
     * @return la position du projectile concernant la largeur de la fenêtre
     */
    public double getPosY() {
        return this.positionY;
    }
    /** Permet de récupérer le dessin du projectile
     * @return le dessin du projectile
     */
    public EnsembleChaines getEnsembleChaines() {
        this.dessin = new EnsembleChaines();
        this.dessin.ajouteChaine((int) positionX, (int) positionY, "█");  
        return this.dessin;
    }
    /** Permet de faire évoluer le projectile */
    public void evolue() {
        this.positionY -= 0.5;
        for (ChainePositionnee c : this.dessin.chaines) {
            c.y = (int) this.positionY;
        }
    }
}
