public class AlienRapide {
    /** Cet attribut modélise la position de l'alien rapide concernant la largeur de la fenêtre */
    private double positionX;
    /** Cet attribut modélise la position de l'alien rapide concernant la hauteur de la fenêtre */
    private double positionY;
    /** Cet attribut modélise le dessin de l'alien rapide */
    private EnsembleChaines dessin;
    /** Cet attribut modélise la direction du déplacement de l'alien rapide */
    private double direction;
    /** Cet attribut modélise un compteur de tours */
    private int compteurTours;
    /** Cet attribut modélise l'animation de l'alien rapide */
    public boolean sAnime;
    /** Permet de construire l'alien rapide
     * @param positionX la position de l'alien rapide concernant la largeur de la fenêtre
     * @param positionY la position de l'alien rapide concernant la hauteur de la fenêtre
     */
    public AlienRapide(double positionX, double positionY) {
        this.dessin = new EnsembleChaines();
        this.positionX = positionX;
        this.positionY = positionY;
        this.compteurTours = 0;
        this.direction = 1;
        this.sAnime = false;
    }
    /** Permet de récupérer le dessin de l'alien rapide
     * @return le dessin de l'alien rapide
     */
    public EnsembleChaines getEnsembleChaines() {
        this.dessin = new EnsembleChaines();
        this.dessin.ajouteChaine((int) positionX, (int) positionY, "    ▄▄████▄▄     ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-1, "   ██████████    ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-2, "   ██▄▄██▄▄██    ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-3, "    ▄▀▄▀▀▄▀▄     ");
        if (this.sAnime) {
            this.dessin.ajouteChaine((int) positionX, (int) positionY-4, "   ▀        ▀    ");
        } else {
            this.dessin.ajouteChaine((int) positionX, (int) positionY-4, "      ▀  ▀       ");
        }
        return this.dessin;
    }
    /** Permet de récupérer la position de l'alien rapide concernant la largeur de la fenêtre
     * @return la position de l'alien rapide concernant la largeur de la fenêtre
     */
    public double getPositionX() {
        return this.positionX;
    }
    /** Permet de récupérer la position de l'alien rapide concernant la hauteur de la fenêtre
     * @return la position de l'alien rapide concernant la hauteur de la fenêtre
     */
    public double getPositionY() {
        return this.positionY;
    }
    /** Permet de récupérer la direction du déplacement de l'alien rapide
     * @return la direction du déplacement de l'alien rapide
     */
    public void evolue() {
        if (this.compteurTours == 80) {
            this.compteurTours = 0;
            --this.positionY;
            direction = -direction;
            for (int i = 0; i < this.dessin.chaines.size(); ++i) {
                this.dessin.chaines.get(i).x = (int) this.positionX;
                this.dessin.chaines.get(i).y = i == 0 ? (int) this.positionY : (int) this.positionY - i; 
            }
        }
        if (this.compteurTours % 10 == 0) {
            this.sAnime = !this.sAnime;
        }
        this.positionX += direction;
        for (ChainePositionnee c : this.dessin.chaines) {
            c.x = (int) this.positionX;
        }
        ++this.compteurTours;
    }
    /** Permet de savoir si l'alien rapide est touché par un tir
     * @param x la position du tir concernant la largeur de la fenêtre
     * @param y la position du tir concernant la hauteur de la fenêtre
     * @return true si l'alien rapide est touché par un tir, false sinon
     */
    public boolean contient(int x, int y) {
        return this.dessin.contient(x,y);
    }
}
