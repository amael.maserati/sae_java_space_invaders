import java.util.ArrayList;
import java.util.List;

public class AlienTireur {
    /** Cet attribut modélise la position de l'alien tireur concernant la hauteur de la fenêtre */
    private double positionX;
    /** Cet attribut modélise la position de l'alien tireur concernant la largeur de la fenêtre */
    private double positionY;
    /** Cet attribut modélise le dessin de l'alien tireur */
    private EnsembleChaines dessin;
    /** Cet attribut modélise la direction de l'alien tireur */
    private double direction;
    /** Cet attribut modélise un compteur de tours */
    private int compteurTours;
    /** Cet attribut modélise l'animation de l'alien tireur */
    private boolean sAnime;
    /** Cet attribut modélise la liste des projectiles tirés par les aliens */
    private List<ProjectileAlienTireur> projectilesAliensTireurs;
    /** Cet attribut modélise la liste des projectiles tirés par les aliens qui ont été ratés */
    private List<ProjectileAlienTireur> projectilesAliensTireursRates;
    /** Cet attribut modélise un compteur pour tous les 100 tours */
    private int compteur100Tours;
    /** Permet de construire l'alien tireur
     * @param positionX la position de l'alien tireur concernant la hauteur de la fenêtre
     * @param positionY la position de l'alien tireur concernant la largeur de la fenêtre
     */
    public AlienTireur(double positionX, double positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.dessin = new EnsembleChaines();
        this.direction = 0.1;
        this.compteurTours = 0;
        this.sAnime = false;
        this.projectilesAliensTireurs = new ArrayList<ProjectileAlienTireur>();
        this.projectilesAliensTireursRates = new ArrayList<ProjectileAlienTireur>();
        this.compteur100Tours = 0;
    }
    /** Permet de récupérer la liste des projectiles tirés par les aliens
     * @return la liste des projectiles tirés par les aliens
     */
    public EnsembleChaines getEnsembleChaines() {
        this.dessin = new EnsembleChaines();
        this.dessin.ajouteChaine((int) positionX, (int) positionY, "      ▄██▄       ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-1, "    ▄██████▄     ");
        this.dessin.ajouteChaine((int) positionX, (int) positionY-2, "   ███▄██▄███    ");
        if (this.sAnime) {
            this.dessin.ajouteChaine((int) positionX, (int) positionY-3, "    ▄▀▄▄▀▄       ");
            this.dessin.ajouteChaine((int) positionX, (int) positionY-4, "   ▀ ▀  ▀ ▀      ");
        } else {
            this.dessin.ajouteChaine((int) positionX, (int) positionY-3, "      ▄▀▄▄▀▄     ");
            this.dessin.ajouteChaine((int) positionX, (int) positionY-4, "     ▀ ▀  ▀ ▀     ");
        }
        return this.dessin;
    }
    /** Permet de récupérer la position de l'alien tireur concernant la hauteur de la fenêtre
     * @return la position de l'alien tireur concernant la hauteur de la fenêtre
     */
    public double getPositionX() {
        return this.positionX;
    }
    /** Permet de récupérer la position de l'alien tireur concernant la largeur de la fenêtre
     * @return la position de l'alien tireur concernant la largeur de la fenêtre
     */
    public double getPositionY() {
        return this.positionY;
    }
    /** Permet de récupérer la direction de l'alien tireur
     * @return la direction de l'alien tireur
     */
    public void evolue() {
        if (this.compteurTours == 100) {
            ++this.compteur100Tours;
            this.compteurTours = 0;
            --this.positionY;
            this.direction = -this.direction;
            for (int i = 0; i < this.dessin.chaines.size(); ++i) {
                this.dessin.chaines.get(i).x = (int) this.positionX;
                this.dessin.chaines.get(i).y = i == 0 ? (int) this.positionY : (int) this.positionY - i;
                if (this.compteur100Tours == 2) {
                    this.projectilesAliensTireurs.add(new ProjectileAlienTireur(this.dessin.chaines.get(i).x + 8, this.dessin.chaines.get(0).y - 1));
                    this.compteur100Tours = 0;
                } 
            }
        }
        if (this.compteurTours % 10 == 0) {
            this.sAnime = !this.sAnime;
        }
        this.positionX += direction;
        for (ChainePositionnee c : this.dessin.chaines) {
            c.x = (int) this.positionX;
        }
        for (ProjectileAlienTireur p : this.projectilesAliensTireurs) {
            if (p.getPosY() >= 0) {
                p.evolue();
            }
            else {
                this.projectilesAliensTireursRates.add(p);
            }
        }
        ++compteurTours;
    }
    /** permet de savoir si l'élément d'un dessin de l'alien tireur contient un autre dessin
     * @param x la position de l'élément du dessin concernant la hauteur de la fenêtre
     * @param y la position de l'élément du dessin concernant la largeur de la fenêtre
     * @return true si l'alien tireur contient l'élément du dessin, false sinon
     */
    public boolean contient(int x, int y) {
        return this.dessin.contient(x,y);
    }
    /** Permet de récupérer la liste des projectiles tirés par les aliens
     * @return la liste des projectiles tirés par les aliens
     */
    public List<ProjectileAlienTireur> getProjectilesAliensTireurs() {
        return this.projectilesAliensTireurs;
    }
    /** Permet de récupérer la liste des projectiles tirés par les aliens n'ayant pas touché le vaisseau
     * @return la liste des projectiles tirés par les aliens qui n'ayant pas touché le vaisseau
     */
    public List<ProjectileAlienTireur> getProjectilesAliensTireursRates() {
        return this.projectilesAliensTireursRates;
    }
}
